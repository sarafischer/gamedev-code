import time
from itertools import cycle

ORANGE = (255, 140, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
colors = [ORANGE, RED, WHITE, GREEN, ORANGE]


# my solution:
start_time = time.time()
index = 0

for i in range(100000):
    ball_color = colors[index]
    index += 1
    if index == 4:
        index = 0

duration = time.time() - start_time
print("My version: " + str(duration) + " seconds")


# Jonas' solution:
start_time = time.time()
collision_counter = 0
color_length = len(colors)

for i in range(100000):
    if collision_counter < color_length:
        ball_color = colors[collision_counter]
    else:
        ball_color = colors[collision_counter % color_length]
    collision_counter += 1

duration = time.time() - start_time
print("Jonas' version: " + str(duration) + " seconds")


# Patricias solution:
start_time = time.time()

for i in range(100000):
    all_colors_2 = cycle(colors)
    order_list_color = next(all_colors_2)

duration = time.time() - start_time
print("Patricias version: " + str(duration) + " seconds")

